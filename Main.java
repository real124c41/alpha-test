import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import static java.nio.file.Files.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.StandardOpenOption;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.io.IOException;
import java.math.BigInteger;

public class Main {
    private final static Path path = Paths.get("test.csv");

    private static void makeFile() throws IOException {
        List<Integer> range = IntStream.range(0, 21).boxed().collect(Collectors.toList());
        Collections.shuffle(range);
        write(path, range.stream().map(i -> i.toString()).collect(Collectors.joining(",")).getBytes());
        System.out.print("Created test file 'test.csv' with sequence");
        System.out.print("    ");
        System.out.println(range);
    }
    
    private static List<List<Integer>> readTestFile() throws IOException {
        return lines(path)
            .map(s -> Arrays.asList(s.split(",")).stream()
                .map(i -> Integer.parseInt(i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }

    private static void readSortPrint() throws IOException {
        List<Integer> numbers = readTestFile().get(0);
        Collections.sort(numbers);
        System.out.print("Readed and sorted test file 'test.csv' to sequence");
        System.out.print("    ");
        System.out.println(numbers);
    }
    
    private static void readReversePrint() throws IOException {
        List<Integer> numbers = readTestFile().get(0);

        Collections.reverse(numbers);
        System.out.print("Readed and reversed test file 'test.csv' to sequence");
        System.out.print("    ");
        System.out.println(numbers);
    }

    private static BigInteger factorialRecurse(Integer n){
        if (n <= 0) return BigInteger.ONE;
        BigInteger result = BigInteger.valueOf(n);
        return result.multiply(factorialRecurse(n-1));
    }

    private static BigInteger factorialIter(Integer n){
        if (n <= 0) return BigInteger.ONE;
        BigInteger result = BigInteger.ONE;
        for (int i = 1; i <= n; ++i) result = result.multiply(BigInteger.valueOf(i));
        return result;
    }

    public static void main(String[] args){
        try {
            System.out.println("Create test file 'test.csv'");
            makeFile();
            System.out.println("Read sort and print sequence from test file 'test.csv'");
            readSortPrint();
            System.out.println("Read reverse and print sequence from test file 'test.csv'");
            readReversePrint();
        } catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("Calculate factorial recursively for: 20");
        System.out.print("    Result: ");
        System.out.println(factorialRecurse(20));
        System.out.println("Calculate factorial iteratively for: 20");
        System.out.print("    Result: ");
        System.out.println(factorialIter(20));
    }
}
